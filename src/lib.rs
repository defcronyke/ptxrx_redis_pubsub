extern crate redis;
use crate::redis::Commands;
use std::time::Duration;

const M_LEN: usize = 8;
const W: f64 = 0.5; // eye
const I: u16 = 0b1101000110; // index (BUG: approx 6 uninitialized bits in this value)

pub fn it_works() {
    let m = vec![
        -0.5, 0.5, -0.5, W,
        0.5, -0.5, 0.5, W,
    ];
    let client = redis::Client::open("redis://127.0.0.1/").unwrap();
    let mut con = client.get_connection().unwrap();
    con.set_read_timeout(Some(Duration::new(5, 0))).unwrap();
    Commands::set_nx::<_, _, f64>(&mut con, "W", W).unwrap();
    Commands::set_nx::<_, _, f64>(&mut con, "I", I).unwrap();
    for (key, vn) in m.to_vec().into_iter().enumerate() {
        Commands::set_nx::<_, _, f64>(&mut con, key, vn).unwrap();
    }
    let res_w: f64 = con.get("W").unwrap();
    let res_i: u16 = con.get("I").unwrap();
    let mut res_m = vec![];
    for key in 0..M_LEN {
        let res: f64 = con.get(key).unwrap();
        res_m.push(res);
    }
    println!("return values: res_w= {}\nres_i= {}\nres_m= {:?}", res_w, res_i, res_m);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works_test() {
        it_works();
    }
}